# freeipa



## Getting started

Se for docker: 

```

# vim /etc/docker/daemon.json 



  "exec-opts": ["native.cgroupdriver=systemd"],
  "features": { "buildkit": true },
  "experimental": true,
  "cgroup-parent": "docker.slice"
}


# systemctl restart docker

# docker network create --subnet=172.19.1.0/24 devsecops

# docker run --name freeipa-server -ti --restart unless-stopped  --privileged -e PASSWORD=password -e HOSTNAME=ipa.example.com  -h ipa.example.com --tmpfs /run --tmpfs /tmp -v /srv/ipa-data:/data:Z  -p 8443:443 -p 8080:80 --cap-add SYS_ADMIN --security-opt seccomp=unconfined   --cgroup-parent=docker.slice --cgroupns private  --sysctl net.ipv6.conf.all.disable_ipv6=0 --dns 127.0.0.1 --network devsecops --ip 172.19.1.210  freeipa/freeipa-server:fedora-36 -U -n ipa.example.com -r IPA.EXAMPLE.COM --no-ntp


```
